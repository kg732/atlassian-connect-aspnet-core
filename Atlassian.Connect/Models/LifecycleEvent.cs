﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlassian.Connect.Entities
{
    public class LifecycleEvent
    {
        /// <summary>
        /// App key that was installed into the Atlassian Product, as it appears in your app's descriptor.
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Identifying key for the Atlassian product tenant that the app was installed into.
        /// </summary>
        /// <remarks>
        /// This will never change for an Atlassian product tenant. However, be aware that importing data into a site will result in a new tenant. In this case, any app installations into the new tenant (using the same baseUrl as the previous tenant) will result in an installation payload containing a different clientKey. Determining the contract between the instance and app in this situation is tracked by AC-1528 in the Connect backlog.
        /// </remarks>
        public string ClientKey { get; set; }
        /// <summary>
        /// The account ID for identifying users in the `sub` claim sent in JWT during calls. This is the user associated with the relevant action, and may not be present if there is no logged in user.
        /// </summary>
        public string AccountId { get; set; }
        /// <summary>
        /// Use this string to sign outgoing JWT tokens and validate incoming JWT tokens. Optional: and may not be present on non-JWT app installations, and is only sent on the installed event. All instances of your app use the same shared secret.
        /// </summary>
        public string SharedSecret { get; set; }
        /// <summary>
        /// URL prefix for this Atlassian product instance. All of its REST endpoints begin with this `baseUrl`
        /// </summary>
        public string BaseUrl { get; set; }
        /// <summary>
        /// If the Atlassian product instance has an associated custom domain, this is the URL through which users will access the product. Any links which an app renders server-side should use this as the prefix of the link. This ensures links are rendered in the same context as the remainder of the user's site.
        /// 
        /// This field may not be present, in which case the baseUrl value should be used.API requests from your App should always use the baseUrl value and not be based on the URL specified here.
        /// </summary>
        public string DisplayUrl { get; set; }
        /// <summary>
        /// If the Atlassian product instance has an associated custom domain for Jira Service Desk functionality, this is the URL for the Jira Service Desk Help Center. Any related links which an app renders server-side should use this as the prefix of the link. This ensures links are rendered in the same context as the user's Jira Service Desk.
        /// 
        /// This field may not be present, in which case the baseUrl value should be used.API requests from your App should always use the baseUrl value and not be based on the URL specified here.
        /// </summary>
        public string DisplayUrlServicedeskHelpCenter { get; set; }
        /// <summary>
        /// Identifies the category of Atlassian product, e.g. jira or confluence.
        /// </summary>
        public string ProductType { get; set; }
        /// <summary>
        /// The host product description - this is customisable by an instance administrator.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Also known as the SEN, the service entitlement number is the app license ID. This attribute will only be included during installation of a paid app.
        /// </summary>
        public string ServiceEntitlementNumber { get; set; }
        /// <summary>
        /// installed, uninstalled, enabled, disabled
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// The OAuth 2.0 client ID for your app.
        /// </summary>
        public string OauthClientId { get; set; }
    }

}
