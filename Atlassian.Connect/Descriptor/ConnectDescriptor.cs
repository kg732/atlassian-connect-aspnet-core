using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Atlassian.Connect.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;

namespace Atlassian.Connect.Descriptor
{
	public class ConnectDescriptor : AddOnDescriptor
	{
		public ConnectDescriptor()
		{
			Scopes = new List<Scopes>();
		}

		public bool Validate(bool throwIfInvalid = false)
		{
			bool isValid;

			var json = JsonConvert.SerializeObject(this);

			// Get JSON Schema
			JSchema schema;
			try
			{
				var assembly = typeof(ConnectDescriptor).GetTypeInfo().Assembly;
				var schemaStream = assembly.GetManifestResourceStream("Atlassian.Connect.Resources.ConfluenceGlobalSchema.json");
				var schemaStreamReader = new StreamReader(schemaStream);
				var schemaJsonReader = new JsonTextReader(schemaStreamReader);
				schema = JSchema.Load(schemaJsonReader);
			}
			catch (System.Exception)
			{
				throw new System.Exception("Failed to load ConfluenceGlobalSchema");
			}

			JsonTextReader reader = new JsonTextReader(new StringReader(json));
			JSchemaValidatingReader validatingReader = new JSchemaValidatingReader(reader)
			{
				Schema = schema
			};

			var validationMessages = new SchemaValidationMessageCollection();
			validatingReader.ValidationEventHandler += (o, a) =>
			{
				// Ignore URI formats, since it is incompatible with atlassian-connect samples
				string[] IgnoreFormats = new string[] { "uri", "uri-template" };
				if (IgnoreFormats.Contains(a.ValidationError.Schema.Format))
				{
					return;
				}

				validationMessages.Add(a.Message);
			};

			JsonSerializer serializer = new JsonSerializer();
			ConnectDescriptor descriptor = serializer.Deserialize<ConnectDescriptor>(validatingReader);

			isValid = validationMessages.Count == 0;

			if (!isValid && throwIfInvalid)
			{
				throw new ConnectDescriptorInvalidException(validationMessages.ToString());
			}

			return isValid;
		}

		public static ConnectDescriptor ConvertFromJson(string json)
		{
			return JsonConvert.DeserializeObject<ConnectDescriptor>(json);
		}
	}
}