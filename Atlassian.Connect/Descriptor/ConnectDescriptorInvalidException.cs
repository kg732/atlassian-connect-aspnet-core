﻿namespace Atlassian.Connect.Descriptor
{
	public class ConnectDescriptorInvalidException : System.Exception
	{
		public ConnectDescriptorInvalidException(string validationMessages) : base($"Atlassian Connect Descriptor failed validation. Validation Messages: {validationMessages}") { }
	}
}