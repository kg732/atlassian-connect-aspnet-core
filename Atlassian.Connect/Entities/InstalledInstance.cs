﻿namespace Atlassian.Connect.Entities
{
	public interface IInstalledInstance
	{
		int Id { get; set; }

		string BaseUrl { get; set; }
		string ClientKey { get; set; }
		string SharedSecret { get; set; }
	}

	public class InstalledInstance : IInstalledInstance
	{
		public virtual int Id { get; set; }
		public virtual string BaseUrl { get; set; }
		public virtual string ClientKey { get; set; }
		public virtual string SharedSecret { get; set; }
	}
}
