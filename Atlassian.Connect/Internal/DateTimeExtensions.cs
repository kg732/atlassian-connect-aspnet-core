using System;

namespace Atlassian.Connect.Internal
{
	public static class DateTimeExtensions
	{
		private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		public static long AsUnixTimestampSeconds(this DateTime time)
		{
			if (time > UnixEpoch)
				return (long)(time - UnixEpoch).TotalSeconds;

			return 0L;
		}
	}
}