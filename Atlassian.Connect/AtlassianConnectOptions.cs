﻿using System;
using System.Threading.Tasks;

namespace Atlassian.Connect
{
	public class AtlassianConnectOptions
	{
        public string InstallCallbackPath { get; set; }
        public string UninstallCallbackPath { get; set; }
		public string EnabledCallbackPath { get; set; }
        public string DisabledCallbackPath { get; set; }

        public Func<OnInstallContext, Task> OnInstall { get; set; }
        public Func<OnUninstallContext, Task> OnUninstall { get; set; }
        public Func<OnEnabledContext, Task> OnEnabled { get; set; }
        public Func<OnDisabledContext, Task> OnDisabled { get; set; }

        public string AddOnKey { get; set; }
		public string DescriptorPath { get; set; }
		public bool ValidateDescriptorSchema { get; set; } = true;
	}
}
