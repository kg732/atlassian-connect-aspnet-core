﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Atlassian.Connect.Middleware
{
	public class AtlassianConnectBuilder
	{

		public AtlassianConnectBuilder(IServiceCollection services)
		{
			Services = services ?? throw new ArgumentNullException(nameof(services));
		}

		public IServiceCollection Services { get; }

	}
}
