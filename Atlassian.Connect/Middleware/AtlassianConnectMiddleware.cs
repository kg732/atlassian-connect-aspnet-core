﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Connect.Entities;
using Atlassian.Connect.Persister;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Atlassian.Connect.Middleware
{
    public class AtlassianConnectMiddleware //: IMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly AtlassianConnectOptions _options;

        public AtlassianConnectMiddleware(RequestDelegate next, ILogger<AtlassianConnectMiddleware> logger, AtlassianConnectOptions options)
        {
            _next = next;
            _logger = logger;
            _options = options;
        }

        public async Task Invoke(HttpContext context, IPluginLifecycleManager lifecycleManager, ConnectDescriptorAccessor descriptorProvider)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            // Handle installed path
            if (context.Request.Path.Equals(_options.InstallCallbackPath, StringComparison.OrdinalIgnoreCase))
            {
                await HandleInstalledPostAsync(context, lifecycleManager);
                return;
            }
            // Handle uninstalled path
            if (context.Request.Path.Equals(_options.UninstallCallbackPath, StringComparison.OrdinalIgnoreCase))
            {
                await HandleUninstalledPostAsync(context, lifecycleManager);
                return;
            }
            // Handle enabled path
            if (context.Request.Path.Equals(_options.EnabledCallbackPath, StringComparison.OrdinalIgnoreCase))
            {
                await HandleEnabledPostAsync(context, lifecycleManager);
                return;
            }
            // Handle disabled path
            if (context.Request.Path.Equals(_options.DisabledCallbackPath, StringComparison.OrdinalIgnoreCase))
            {
                await HandleDisabledPostAsync(context, lifecycleManager);
                return;
            }
            // Handle descriptor path
            if (context.Request.Path.Equals(_options.DescriptorPath, StringComparison.OrdinalIgnoreCase))
            {
                await HandleDescriptorGetAsync(context, descriptorProvider);
                return;
            }

            await _next(context);
        }

        private async Task HandleDisabledPostAsync(HttpContext context, IPluginLifecycleManager lifecycleManager)
        {
            if (!HttpMethods.IsPost(context.Request.Method))
            {
                context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                return;
            }

            string bodyText;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8))
            {
                bodyText = await reader.ReadToEndAsync();
            }

            LifecycleEvent @event;
            try
            {
                @event = JsonConvert.DeserializeObject<LifecycleEvent>(bodyText);
            }
            catch (Exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return;
            }

            if (@event.EventType != "disabled")
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return;
            }

            await lifecycleManager.DisablePlugin(context.Request.GetClientKey(), @event);
            context.Response.StatusCode = (int)HttpStatusCode.Created;
        }
        
        private async Task HandleEnabledPostAsync(HttpContext context, IPluginLifecycleManager lifecycleManager)
        {
            _logger.LogInformation("HandleEnabledPostAsync not handled so skipping");
        }
        
        public async Task HandleDescriptorGetAsync(HttpContext context, ConnectDescriptorAccessor descriptorProvider)
        {
            if (!HttpMethods.IsGet(context.Request.Method))
            {
                context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                return;
            }

            var descriptor = descriptorProvider.GetDescriptor(_options.ValidateDescriptorSchema);

            string json = descriptor.ToJson();

            context.Response.StatusCode = (int)HttpStatusCode.OK;
            await context.Response.WriteAsync(json);
        }



        public async Task HandleUninstalledPostAsync(HttpContext context, IPluginLifecycleManager lifecycleManager)
        {
            if (!HttpMethods.IsPost(context.Request.Method))
            {
                context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                return;
            }

            string bodyText;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8))
            {
                bodyText = await reader.ReadToEndAsync();
            }

            IInstalledInstance instanceData = null;
            try
            {
                instanceData = JsonConvert.DeserializeObject<InstalledInstance>(bodyText);
            }
            catch (Exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return;
            }

            if (instanceData != null)
            {
                await lifecycleManager.UninstallPlugin(context.Request.GetClientKey(), instanceData);
                context.Response.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
        }


        public async Task HandleInstalledPostAsync(HttpContext context, IPluginLifecycleManager lifecycleManager)
        {
            if (!HttpMethods.IsPost(context.Request.Method))
            {
                context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                return;
            }

            string bodyText;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8))
            {
                bodyText = await reader.ReadToEndAsync();
            }

            IInstalledInstance instanceData = null;
            try
            {
                instanceData = JsonConvert.DeserializeObject<InstalledInstance>(bodyText);
            }
            catch (Exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return;
            }

            if (instanceData != null)
            {
                await lifecycleManager.InstallPlugin(context.Request.GetClientKey(), instanceData);
                context.Response.StatusCode = (int)HttpStatusCode.Created;
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
        }
    }
}
