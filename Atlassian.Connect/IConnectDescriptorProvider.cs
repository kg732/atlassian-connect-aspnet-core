﻿using Atlassian.Connect.Descriptor;

namespace Atlassian.Connect
{
	public interface IConnectDescriptorProvider
	{
		ConnectDescriptor GetConnectDescriptor();
	}
}