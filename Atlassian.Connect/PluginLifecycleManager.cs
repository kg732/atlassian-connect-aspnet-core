﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Connect.Entities;
using Atlassian.Connect.Persister;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Atlassian.Connect
{
    public class PluginLifecycleManager : IPluginLifecycleManager
    {
        private readonly AtlassianConnectOptions _options;
        private readonly IInstalledInstancePersister _persister;
        private readonly ILogger<PluginLifecycleManager> _logger;

        public PluginLifecycleManager(AtlassianConnectOptions options, IInstalledInstancePersister persister, ILogger<PluginLifecycleManager> logger)
        {
            _options = options;
            _persister = persister;
            _logger = logger;
        }

  

        public async Task UninstallPlugin(string clientKey, IInstalledInstance instanceData)
        {
            _logger.LogDebug("running UninstallPlugin");

            var ctx = new OnUninstallContext
            {
                ClientKey = clientKey,
                InstanceData = instanceData
            };

            if (_options.OnUninstall != null)
            {
                await _options.OnUninstall(ctx);
            }

            if (ctx.PreventAction)
            {
                return;
            }


            await _persister.RemoveClient(clientKey);
        }

        public async Task InstallPlugin(string clientKey, IInstalledInstance instanceData)
        {
            _logger.LogDebug("running InstallPlugin");

            var ctx = new OnInstallContext
            {
                ClientKey = clientKey,
                InstanceData = instanceData
            };

            if (_options.OnInstall != null)
            {
                await _options.OnInstall(ctx);
            }

            if (ctx.PreventAction)
            {
                return;
            }

            await _persister.RemoveClient(clientKey);
        }




        public async Task EnablePlugin(string clientKey, IInstalledInstance instanceData)
        {
            _logger.LogDebug("running EnablePlugin");

            var ctx = new OnEnabledContext
            {
                ClientKey = clientKey,
                InstanceData = instanceData
            };

            if (_options.OnEnabled != null)
            {
                await _options.OnEnabled(ctx);
            }

            if (ctx.PreventAction)
            {
                return;
            }

            await _persister.EnableClient(clientKey);
        }



        public async Task DisablePlugin(string clientKey, LifecycleEvent @event)
        {
            _logger.LogDebug("running DisablePlugin");

            var ctx = new OnDisabledContext
            {
                ClientKey = clientKey,
                Event = @event
            };

            if (_options.OnDisabled != null)
            {
                await _options.OnDisabled(ctx);
            }

            if (ctx.PreventAction)
            {
                return;
            }

            
            
            await _persister.DisableClient(clientKey);
        }

    }

    public class OnInstallContext
    {
        public bool PreventAction { get; set; } = false;
        public string ClientKey { get; set; }
        public IInstalledInstance InstanceData { get; set; }
    }

    public class OnUninstallContext
    {
        public bool PreventAction { get; set; } = false;
        public string ClientKey { get; set; }
        public IInstalledInstance InstanceData { get; set; }
    }

    public class OnEnabledContext
    {
        public bool PreventAction { get; set; } = false;
        public string ClientKey { get; set; }
        public IInstalledInstance InstanceData { get; set; }
    }

    public class OnDisabledContext
    {
        public bool PreventAction { get; set; } = false;
        public string ClientKey { get; set; }
        public LifecycleEvent Event { get; set; }
    }
}
