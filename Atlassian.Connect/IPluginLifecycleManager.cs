﻿using System.Threading.Tasks;
using Atlassian.Connect.Entities;

namespace Atlassian.Connect
{
    public interface IPluginLifecycleManager
    {
        Task UninstallPlugin(string clientKey, IInstalledInstance instanceData);
        Task InstallPlugin(string clientKey, IInstalledInstance instanceData);
        Task EnablePlugin(string clientKey, IInstalledInstance instanceData);
        Task DisablePlugin(string clientKey, LifecycleEvent @event);
    }
}