using System;
using System.Threading.Tasks;
using Atlassian.Connect.Persister;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Atlassian.Connect.Jwt
{
	public class JwtAuthenticationFilterInternal : IAsyncActionFilter
	{
		private readonly IInstalledInstancePersister _persister;
		public JwtAuthenticationFilterInternal(IInstalledInstancePersister persister)
		{
			_persister = persister;
		}

		public async Task OnActionExecutionAsync(ActionExecutingContext filterContext, ActionExecutionDelegate next)
		{
			try
			{
				var requestToken = filterContext.HttpContext.Request.GetJwtToken();
				var clientKey = filterContext.HttpContext.Request.GetClientKey();

				if (String.IsNullOrEmpty(requestToken))
				{
					throw new Exception("Authentication failed, missing JWT token");
				}

				var secretKey = (await _persister.GetFromClientKey(clientKey))?.SharedSecret;

				var token = new EncodedJwtToken(secretKey, requestToken).Decode();
				token.ValidateToken(filterContext.HttpContext.Request);
			}
			catch (Exception)
			{
				filterContext.Result = new UnauthorizedResult();
				return;
			}

			await next();
		}
	}
}