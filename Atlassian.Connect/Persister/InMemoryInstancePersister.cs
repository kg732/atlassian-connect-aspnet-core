﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atlassian.Connect.Entities;

namespace Atlassian.Connect.Persister
{
	public class InMemoryInstalledInstancePersister : IInstalledInstancePersister
	{
		private readonly IList<IInstalledInstance> _instances;

		public InMemoryInstalledInstancePersister()
		{
			_instances = new List<IInstalledInstance>();
		}

		public Task SaveAsync(IInstalledInstance instance)
		{
			_instances.Add(instance);
			return Task.CompletedTask;
		}

		public Task<IInstalledInstance> GetFromClientKey(string clientKey)
		{
			return Task.FromResult(_instances.FirstOrDefault(i => i.ClientKey == clientKey));
		}

        public Task RemoveClient(string clientKey)
        {
            var toRemove = _instances.Where(i => i.ClientKey == clientKey).ToList();
            foreach (var instance in toRemove)
            {
                _instances.Remove(instance);
            }
            
            return Task.CompletedTask;
        }

        public Task EnableClient(string clientKey)
        {
            return Task.CompletedTask;
        }

        public Task DisableClient(string clientKey)
        {
            return Task.CompletedTask;
        }
    }
}
