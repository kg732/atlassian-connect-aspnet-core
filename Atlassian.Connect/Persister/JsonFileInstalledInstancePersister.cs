﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Atlassian.Connect.Entities;

namespace Atlassian.Connect.Persister
{

    public class JsonFileInstalledInstancePersister : IInstalledInstancePersister
    {
        private readonly string _path;
        private readonly SemaphoreSlim _instanceFileLock = new SemaphoreSlim(1);
        private readonly ConcurrentDictionary<string, IInstalledInstance> _inMemoryInstances = new();

        public JsonFileInstalledInstancePersister(string path)
        {
            _path = path;
            LoadJson();
        }


        class JsonFileInstalledInstance : IInstalledInstance
        {
            public int Id { get; set; }
            public string BaseUrl { get; set; }
            public string ClientKey { get; set; }
            public string SharedSecret { get; set; }
        }

        private void LoadJson()
        {
            if (!File.Exists(_path))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_path));
                return;
            }

            var bytes = File.ReadAllBytes(_path);

            var data = System.Text.Json.JsonSerializer.Deserialize<List<JsonFileInstalledInstance>>(bytes);

            foreach (var instance in data)
            {
                _inMemoryInstances[instance.ClientKey] = instance;
            }
        }

        private async Task SaveJson()
        {
            var data = _inMemoryInstances.Values.Select(i => new JsonFileInstalledInstance
            {
                Id = i.Id,
                BaseUrl = i.BaseUrl,
                ClientKey = i.ClientKey,
                SharedSecret = i.SharedSecret
            }).ToList();

            await _instanceFileLock.WaitAsync();
            try
            {
                await using var stream = File.Open(_path, FileMode.Create, FileAccess.Write);

                await System.Text.Json.JsonSerializer.SerializeAsync(stream, data);
            }
            finally
            {
                _instanceFileLock.Release();
            }
        }

        public Task SaveAsync(IInstalledInstance installedInstance)
        {
            _inMemoryInstances[installedInstance.ClientKey] = installedInstance;
            return SaveJson();
        }

        public Task<IInstalledInstance> GetFromClientKey(string clientKey)
        {
            return Task.FromResult(GetFromClientKeySync(clientKey));
        }

        public Task RemoveClient(string clientKey)
        {
            _inMemoryInstances.TryRemove(clientKey, out _);
            
            return SaveJson();
        }

        public Task EnableClient(string clientKey)
        {
            return Task.CompletedTask;
        }

        public Task DisableClient(string clientKey)
        {
            return Task.CompletedTask;
        }

        public IInstalledInstance GetFromClientKeySync(string clientKey)
        {
            return _inMemoryInstances[clientKey];
        }
    }
}
