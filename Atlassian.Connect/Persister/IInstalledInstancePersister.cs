﻿using System.Threading.Tasks;
using Atlassian.Connect.Entities;

namespace Atlassian.Connect.Persister
{
	public interface IInstalledInstancePersister
	{
		Task SaveAsync(IInstalledInstance installedInstance);

		Task<IInstalledInstance> GetFromClientKey(string clientKey);
        Task RemoveClient(string clientKey);
        Task EnableClient(string clientKey);
        Task DisableClient(string clientKey);
    }
}
