﻿using Atlassian.Connect;
using Atlassian.Connect.AutoRegister;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ConfluenceMacro
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddLogging();

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

			services.AddHttpContextAccessor();

			services.AddSelfRegistration(options =>
			{
				options.Host = Configuration.GetValue<string>("atlassian-connect:host");
				options.Product = Configuration.GetValue<string>("atlassian-connect:product");
				options.Username = Configuration.GetValue<string>("atlassian-connect:username");
				options.Password = Configuration.GetValue<string>("atlassian-connect:password");
			});

			services.AddAtlassianConnect<ConnectDescriptorProvider>(opt =>
			{
				opt.InstallCallbackPath = "/installed";
				opt.AddOnKey = "com.example.myaddon.kg73.confluence.local";
				opt.DescriptorPath = "/atlassian-connect.json";
				opt.ValidateDescriptorSchema = true;
			})
			.AddInMemoryPersister();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}

			app.UseStaticFiles();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");

				routes.MapRoute("backgroundColor", "v1/backgroundColor", new { controller = "Home", action = "BackgroundColorAsync" });
			});

			app.UseAtlassianConnect();
		}
	}
}
