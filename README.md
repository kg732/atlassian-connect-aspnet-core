# Atlassian Connect ASP.NET Core

## Status

Recently ported over to ASP.NET Core. Looking for additional maintainers to build out extra features.

## About

Atlassian Connect ASP.NET Core is a framework to build add-ons for Atlassian products using
[Atlassian Connect](http://connect.atlassian.com). Using add-ons you can extend with a new
feature(s), integrate with other systems, and otherwise customize your Atlassian product.

## Licensing

This is licensed under Apache 2.0, see LICENSE.txt for more details.

## Getting started

Install the `Atlassian.Connect.AspNetCore` package via NuGet to an ASP.NET MVC app. Atlassian.Connect.AspNetCore requires ASP.NET Core 2.1.

### Usage
Register the required AtlassianConnect middleware in your ConfigureServices method in Startup.cs
```
public void ConfigureServices(IServiceCollection services)
{
	services.AddAtlassianConnect(opt => 
	{
		opt.InstallCallbackPath = "/installed";
		opt.AddOnKey = "com.example.myaddon.kg73.local";
	})
	.AddInMemoryPersister();
}
```

Make sure to register AtlassianConnect in your application pipeline

```
public void Configure(IApplicationBuilder app)
{
   app.UseAtlassianConnect(); 
}
```
### Persistence
Atlassian Connect ASP.NET Core needs to persist some data for every installed instance of the add on. You may build your own persister using the `IInstalledInstancePersister` interface. You may instead use a built-in persister by calling it's builder method on `services.AddAtlassianConnect`, such as `AddInMemoryPersister`. Additional persisters will be added in the near future, including an EF Core based persister.

### Descriptor

There are some helper classes you can use, if you want, to build your descriptor.

```
[HttpGet]
public IActionResult Descriptor()
{
    var descriptor = new ConnectDescriptor()
    {
        name = "Hello World",
        description = "Atlassian Connect add-on",
        key = "com.example.myaddon",
        vendor = new ConnectDescriptorVendor()
        {
            name = "Example, Inc.",
            url = "http://example.com"
        },
        authentication = new
        {
            type = "jwt"
        },
        lifecycle = new
        {
            installed = "/installed"
        },
        modules = new
        {
            generalPages = new[]
            {
                new
                {
                    url = "/helloworld.html",
                    key = "hello-world",
                    location = "system.top.navigation.bar",
                    name = new
                    {
                        value = "Greeting"
                    }
                }
            }
        }
    };

    descriptor.SetBaseUrlFromRequest(Request);
    descriptor.scopes.Add("READ");

    return Json(descriptor, JsonRequestBehavior.AllowGet);
}
```
`ConnectDescriptor` is a dynamic object, so you can add whatever properties you want to it, as needed.
It has a helper `descriptor.SetBaseUrlFromRequest(Request)` which allows you to dynamically
set the `baseUrl` in the descriptor from the request instead of configuration.

### Validating a call with JWT

Any controller or action can have a filter attribute applied, `[RequireAtlassianConnectJWT]`, that will
validate the JWT claims from Atlassian and return an unauthorized result if the validation fails.

### Making a REST API call from a controller action

If you need to make calls to the REST API, you simply need to include `ConnectClient` in the controller's constructor.
```
private readonly ConnectClient _client;
public HomeController(ConnectClient client)
{
	_client = client;
}
```
The `ConnectClient` is a scoped HttpClient, which automatically handles adding a JWT authentication header and setting the BaseUrl to the calling tenant's URL. 

### Request Extensions
There are `GetClientKey()` and `GetJwtToken()` extension methods on the controller's `Request` property.



## Reporting Issues

Please use the [Atlassian Connect project](https://ecosystem.atlassian.net/browse/AC)
to report issues, bugs, or make feature requests.

## Contributing

We welcome minor bug fixes and they may be accepted via a pull request. Substantial patches will
require a [contributor license agreement](https://developer.atlassian.com/about/atlassian-contributor-license-agreement) on file.
