﻿using NJsonSchema;
using NJsonSchema.CodeGeneration.CSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Atlassian.Connect.Models.Generator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Read JSON Schema
            var schemaJson = await File.ReadAllTextAsync(Path.Combine(Directory.GetCurrentDirectory(), "ConfluenceGlobalSchema.json"));
            var schema = await JsonSchema.FromJsonAsync(schemaJson);
            var enums = schema.Definitions.Where(kvp => kvp.Value.IsEnumeration);
            var settings = new CSharpGeneratorSettings()
            {
                Namespace = "Atlassian.Connect.Models",
                ClassStyle = CSharpClassStyle.Poco,
                EnumNameGenerator = new DefaultEnumNameGenerator(),
                GenerateJsonMethods = true,
                TypeNameGenerator = new CustomTypeNameGenerator()
            };
            var generator = new CSharpGenerator(schema, settings);

            // Write output
            var file = generator.GenerateFile();
            using (var writer = File.CreateText("../../../../Atlassian.Connect.Models/Output.cs"))
            {
                writer.Write(file);
            }

            Console.WriteLine("Models Generated from Json Schema");
        }
    }

    public class CustomTypeNameGenerator : DefaultTypeNameGenerator
    {


        /// <inheritdoc />
        public override string Generate(JsonSchema schema, string typeNameHint, IEnumerable<string> reservedTypeNames)
        {
            if (string.IsNullOrEmpty(typeNameHint))
            {
                typeNameHint = schema.Title;
            }
            if (string.IsNullOrEmpty(typeNameHint) && !string.IsNullOrEmpty(schema.DocumentPath))
            {
                typeNameHint = schema.DocumentPath.Replace("\\", "/").Split('/').Last();
            }
            return ConversionUtilities.ConvertToUpperCamelCase(typeNameHint
                    .Replace(":", "-").Replace(@"""", @""), true)
                .Replace(".", "_")
                .Replace("#", "_")
                .Replace("-", "_")
                .Replace("_", "")
                .Replace("\\", "_");
        }
    }
}