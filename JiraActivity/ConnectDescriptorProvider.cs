﻿using Atlassian.Connect;
using Atlassian.Connect.Models;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;

namespace JiraActivity
{
	public class ConnectDescriptorProvider : IConnectDescriptorProvider
	{
		private readonly IHostingEnvironment _env;

		public ConnectDescriptorProvider(IHostingEnvironment env)
		{
			_env = env;
		}

		public ConnectDescriptor GetConnectDescriptor()
		{
			return new ConnectDescriptor()
			{
				Name = "Hello World",
				Description = "Atlassian Connect add-on",
				Key = "com.example.myaddon.kg73.local",
				Vendor = new Vendor()
				{
					Name = "Example, Inc.",
					Url = new System.Uri("http://example.com")
				},
				Authentication = new Authentication()
				{
					Type = AuthenticationType.Jwt
				},
				Modules = new Modules()
				{
					GeneralPages = new List<PageModule>()
					{
						new PageModule()
						{
							Url = "/helloworld.html",
							Key = "hello-world",
							Location = "system.top.navigation.bar",
							Name = new I18nProperty(){ Value = "Greeting" }
						}
					}
				}
			};
		}
	}
}
